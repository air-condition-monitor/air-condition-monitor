#!/bin/sh
docker-compose -f production.yml down
docker volume rm acm_static_dir
docker-compose -f production.yml build --no-cache
docker-compose -f production.yml up -d
