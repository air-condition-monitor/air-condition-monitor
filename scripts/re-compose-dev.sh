#!/bin/sh
docker-compose down
docker volume rm acm_static_dir
docker-compose -f development.yml build --no-cache
docker-compose -f development.yml up -d
