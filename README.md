# Air Condition Monitor

Top level repository for the project. Includes:
- project wide configs
- project wide logs
- docker container management (docker-compose)
- docker container setups for influxdb and nginx


## How to run

```
git submodule update --init --recursive
sudo docker-compose up --build
```



## Setup auto-deploy server


- ssh to server


- clone this repo to desired deployment location, e.g. /srv/http/


- create repository for handling push-to-deploy
```
git init --bare path/to/hook/repo

cp {this-repo}/scripts/post-receive path/to/hook/repo
```
- edit paths in script if necessary



- allow usage of {this-repo}/scipts/re-compose.sh without writing password after push:

    - `sudo visudo` and add under line ` %sudo   ALL=(ALL:ALL) ALL` the following:

    ```
    %sudo   ALL=(ALL:ALL) NOPASSWD: {this-repo}/scripts/re-compose.sh
    ```


- open terminal in development computer


- add production repo as remote:
```
git remote add {name} {user}${remote ip/hostname}:/path/to/hook/repo

```
