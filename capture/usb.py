import serial
from datetime import datetime
import time
import sys
from functools import partial
import configparser
import os
import logging
import logging.config
import toml
import requests

PORT = "/dev/ttyUSB0"
DEVICE = "arduino"
LOCATION = "inside"
CAPTURE_INTERVAL = 600  # seconds

LOG_FILE = "./usb.log"
URL = "http://127.0.0.1:5000/api/points"


log_config = toml.load("./log.toml", _dict=dict)["logger"]
log_config["handlers"]["file"]["filename"] = LOG_FILE

# create empty file
if not os.path.exists(log_config["handlers"]["file"]["filename"]):
    with open(log_config["handlers"]["file"]["filename"], "w") as f:
        pass

logging.config.dictConfig(log_config)
log = logging.getLogger()


"""
Reads data from usb port and stores it into file


FORMAT:

yyyy-mm-dd hh:mm:ss tt.tttttt DEVICE

"""

def write_data(points):
    print(f"writing ${points}")

    if len(points) == 0:
        return

    try:
        res = requests.post(URL, json=points)

        if not res.ok:
            log.error("failed to write to db")

    except Exception as e:
        log.exception("write failed")



def main():
    log.info("connecting sensor device \"{}\"".format(DEVICE))

    try:
        # baud 9600, 8 databits, no parity, 1 stopbit
        arduino = serial.Serial(PORT)
        arduino.flushInput()

    except ValueError:
        log.exception("invalid value")

    except serial.SerialException:
        log.exception("unable to connect to sensor device")


    begin_ticks = time.time()

    while True:
        try:
            data = arduino.readline().decode("utf-8")
            data = data[0:len(data)-1]
            timestamp = datetime.now().replace(microsecond=0).isoformat(' ')

            point = {
                "device": DEVICE,
                "location": LOCATION,
                "time": timestamp,
                "temperature": float(data),
            }


            diff = time.time() - begin_ticks

            if diff > CAPTURE_INTERVAL:
                begin_ticks += diff
                write_data([point])

        except KeyboardInterrupt as e:
            log.exception("keyboard interrupt")
            break


if __name__ == "__main__":
    main()

