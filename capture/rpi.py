from http.server import BaseHTTPRequestHandler, HTTPServer
import sys
import os
import requests

import signal
import json
from datetime import datetime
import time
from functools import partial
import configparser
import logging
import logging.config
import toml


"""
ruuvidata:
{
    'temperature': 26.75,
    'humidity': 42.785,
    'pressure': 98155.0,
    'acceleration': {
        'x': 0.048,
        'y': 0.028,
        'z': 0.992
    },
    'voltage': 3.085,
    'datetime': '2020-08-23T23:51:15.930253',
    'mac': 'FA50248D8250'
}
"""

URL = "http://127.0.0.1:5000/api/points"
CAPTURE_INTERVAL = 600  # seconds
DEVICE = "ruuvi"
LOCATION = "inside"
MAC = "FA50248D8250"

log_config = toml.load("./log.toml", _dict=dict)["logger"]

path = log_config["handlers"]["file"]["filename"]
logfile = path.split("/")[-1]
log_config["handlers"]["file"]["filename"] = os.getcwd() + "/" + logfile

if not os.path.exists(log_config["handlers"]["file"]["filename"]):
    try:
        index = log_config["handlers"]["file"]["filename"].rfind("/")
        dir_path = log_config["handlers"]["file"]["filename"][0:index]
        os.makedirs(dir_path)

    except:
        pass

    finally:
        # create empty file
        with open(log_config["handlers"]["file"]["filename"], "w") as f:
            pass

logging.config.dictConfig(log_config)
# TODO: fix log.config so that default root can be used instead
# https://docs.python.org/3/library/logging.config.html
log = logging.getLogger("root")

# disable info logging for http requests
logging.getLogger("urllib3").setLevel(logging.ERROR)


begin_ticks = time.time()


class Server(BaseHTTPRequestHandler):

    def __init__(self, *args, **kwargs):
        BaseHTTPRequestHandler.__init__(self, *args, **kwargs)

    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/json')
        self.end_headers()

    def log_message(self, *args, **kwargs):
        pass

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length).decode('utf-8')

        try:
            json_obj = json.loads(body)

            if json_obj["mac"] == MAC:
                point = {
                    "device": DEVICE,
                    "location": LOCATION,
                    "time": json_obj["datetime"],
                    "temperature": json_obj["temperature"],
                    "humidity": json_obj["humidity"],
                    "pressure": json_obj["pressure"],
                }

                global begin_ticks
                diff = time.time() - begin_ticks

                if diff > CAPTURE_INTERVAL:
                    begin_ticks += diff

                    res = requests.post(URL, json=[point])
                    if not res.ok:
                        log.error("failed to send data")


        except Exception as e:
            log.exception("")

        self._set_response()
        #self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))


def run(server_class=HTTPServer, handler_class=Server, port=8080):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    log.info(f"Starting http server on port {port}...")

    try:
        httpd.serve_forever()

    except KeyboardInterrupt:
        log.info('Keyboard interrupt, stopping...')
        httpd.server_close()


if __name__ == "__main__":

    if len(sys.argv) == 2:
        run(port=int(argv[1]))
    else:
        run()

